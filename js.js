var input = document.querySelector('input[type="file"]');
var words = [];
var appearanceFreq = [];

input.addEventListener('change', function(e) {
    var reader = new FileReader();
    var items = [];
    reader.readAsText(input.files[0])
    reader.onload = function() {
        words = reader.result.split(' ')
        splitFunc();
    }
}, false)

var index = 0;

function splitFunc() {
    for (var i = 0; i < words.length; i++) {
        var wordLetters = words[i].split('');
        if(wordLetters.length > 3){
            processLetters(wordLetters)
        }
    };
    calculatePercent();
    sortMostCommon();
    addToTable();
    addToBarChart();
}
var startIndex = 0;
var maxLetter = 3;
var maxSearchDone = false;
var processingFinished = 0;

function processLetters(letterList) {
    var combination = "";
    for (var j = startIndex; j < letterList.length; j++) {
        if (j <= maxLetter || maxSearchDone) {
            combination += letterList[j];
        }
    }
    addToUniqueList(combination)

    if (maxLetter < letterList.length - 1 && !maxSearchDone) {
        maxLetter++;
        processLetters(letterList);
    } else {
        maxSearchDone = true;
        maxLetter = 3;
    }

    if (startIndex + 4 < letterList.length && maxSearchDone) {
        startIndex++;
        processLetters(letterList)
    } else {
        startIndex = 0;
        maxSearchDone = false;
    }
}

function addToUniqueList(item) {
    var isPresent = false;
    var combinationObj = {
        word: item,
        frequency: 1,
        percent: 0
    }

    for (var i = 0; i < appearanceFreq.length; i++) {
        if (item == appearanceFreq[i].word) {
            isPresent = true;
            appearanceFreq[i].frequency++;
        }
    }
    if (!isPresent) {
        appearanceFreq.push(combinationObj);
    }
}

function calculatePercent() {

    var total = 0;
    for (var i = 0; i < appearanceFreq.length; i++) {
        total += appearanceFreq[i].frequency;
    }

    for (var j = 0; j < appearanceFreq.length; j++) {
        appearanceFreq[j].percent = Math.round(appearanceFreq[j].frequency * 100 / total * 100) / 100
    }
}

function sortMostCommon() {
    appearanceFreq.sort((a, b) => (a.frequency < b.frequency) ? 1 : -1);
}

function addToTable() {
    for (var i = 0; i < appearanceFreq.length; i++) {
        if (i < 10) {
            var tbodyRef = document.getElementById('freqTable').getElementsByTagName('tbody')[0]
            var newRow = tbodyRef.insertRow();
            var combinationCell = newRow.insertCell();
            var frequencyCell = newRow.insertCell();
            combinationCell.append(appearanceFreq[i].word);
            frequencyCell.append(appearanceFreq[i].percent);
        }
    }
}

function addToBarChart() {
    var labelsData = [];
    var percentData = [];

    for (var i = 0; i < appearanceFreq.length; i++) {
        if (i < 10) {
            labelsData.push(appearanceFreq[i].word);
            percentData.push(appearanceFreq[i].percent);
        }
    }
    var barChart = new Chart(document.getElementById('freqBarChart'), {
        type: 'bar',
        data: {
            labels: labelsData,
            datasets: [{
                label: "Appearance Frequency in %",
                backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#00ff99", "#ff0066", "#663300", "#ff8c1a", "#0000ff"],
                data: percentData
            }]
        },
        options: {
            legend: {
                display: false
            },
            title: {
                display: true,
                text: 'Letter combination frequency'
            }
        }
    })
}
